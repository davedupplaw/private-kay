package uk.dupplaw.david.privatekay.client

class PrivateKayClient(
        private val server: String = "localhost",
        private val apiPort: Int = 8081,
        private val managementPort: Int = 8082
) {
    fun getVersion() =
            khttp.get("http://localhost:$managementPort/api/version").text

    fun makeProxy(urlToProxyTo: String, path: String, method: String) =
            khttp.post(
                    url  = "http://localhost:$managementPort/api/proxy/$method$path",
                    data = urlToProxyTo
            ).text

    fun makeMock(response: String, path: String, statusCode: Int, method: String) =
            khttp.post(
                    url = "http://localhost:$managementPort/api/mock/$method/$statusCode$path",
                    data = response
            ).text

    fun clearInteractions() =
            khttp.delete("http://localhost:$managementPort/api/interactions").text

    fun clearAllMappings() =
            khttp.delete("http://localhost:$managementPort/private-kay/api/mappings").text

    fun makeRequest(path: String, method: String) =
            khttp.request(method, "http://localhost:$apiPort$path").text
}
