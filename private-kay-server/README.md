![Private Kay](private-kay.svg)
# Private Kay

Private Kay is an integration testing tool written in Kotlin.  It can be used from Kotlin, Java
or other languages as it is a stand-alone service.

*Note: much of what's described below is not yet implemented; this is a bit like a design document*

## Integration Testing

Integration testing is a term that means taking several components and trying them together.
Private Kay is useful for testing several components that are separated by network connections;
often known as system or end-to-end testing too.

Private Kay can work in a number of different ways and allows you to check, manipulate and mock
responses to and from or between services.  As Private Kay runs as a separate service, you can
proxy your calls through Private Kay and she will forward on your requests as is or respond
in a predefined way.

### Basic Integration Testing

A basic integration testing setup (without Private Kay) might look something like this:

```
      A -> B
      ^   /
Apply |  v  Check
      Test
```

A test service, `Test`, spins up services `A` and `B` and then applies some data at `A` to
check an expected effect at `B`.

This is a black-box testing approach to integration testing and fits a behaviour-driven development
approach. "Given I apply *x* at `A`, *y* will be seen at `B`".

### Proxy Setup

It's likely that service `A` has been unit and functional tested such that the data output component
of `A` is already covered. However, you may wish to ensure that it behaves as expected when deployed
as a service.  Adding a proxy in the middle of `A` and `B` gives you that confidence that the data
being output from `A` is as expected:

```
      A --> Proxy --> B
      ^      | check /
Apply |      v      v  Check
      ---- Test ----
```

`B` should also be unit and functionally tested to ensure that the input data is validated.
You can use the proxy to manipulate the data from `A` to give confidence that `B` behaves as
expected.

Private Kay also provides proxying for TCP-socket integrations, as well as HTTP integrations.

### External Service Mocks

Sometimes, service `B` will require data from an external service, `E`. Private Kay can be set up to
provide static responses to certain inputs from `B`, thereby mocking `E`.

```
      A -> B <---- E
      ^   /        ^
Apply |  v check  / control
      ---- Test --
```

### Complete Control

Private Kay can be used to provide mocking, proxying and testing of network interfaces.

```
      A --> Proxy --> B <---- E
      ^    |   ^      /        ^
Apply |    v   |     v check  / control
      ---------- Test --------
```

# Server Setup

Private Kay is a service; it is the proxy or external source in the above diagrams, so it needs to be deployed along with the
services to test.  The services that are being tested also need have their runtime configuration to be altered to point to
Private Kay where necessary.

The easiest way to run Private Kay is with Docker. Spin up the `davedupplaw/private-kay` image to a container and you'll be ready. You
need to ensure that the ports you will be testing or proxying through are exposed from your container along with the Private Kay
configuration port, which is used by the JUnit rule to set up the server.

```sh
docker run -d -p 9000:9000 davedupplaw/private-kay
```
or with docker-compose:
```yaml
private-kay:
  image: davedupplaw/private-kay
  ports:
    - "9000:9000"
```

You could also set up the Private Kay server as part of your test using [test-containers](https://github.com/testcontainers/testcontainers-java), for example.

# Junit

Private Kay has an interface for control of an external Private Kay server within your JUnit tests.
The `@Rule` or extension for Private Kay takes a setup and applies it to the server at the given host/port. 
Note that Private Kay has its own REST interface at the given port which is used to control 
it during integration testing.

The following examples set up 4 mock endpoints (representing two different services), an HTTP proxy and a TCP-socket proxy.

## Junit 5

```java
@ExtendWith(PrivateKay.class)
public class TestClass {
      @PrivateKayFixture public PrivateKay privateKay;

      @BeforeAll
      public void setup() {
            privateKay.withServer("localhost", 9090)
                  .with(new HttpProxy(8080, "localhost:8081", "serviceA_to_serviceB"))
                  .with(new SocketProxy(8070, "localhost", 8071, "socketA_to_socketB"))
                  .with(new MockService(8090)
                        .with(new MockEndpoint("/api/version").when(GET).return("<version>2.1.3</version>", 200)
                        .with(new MockEndpoint("/api/error").when(ANY).return(500)
                        .with(new MockEndpoint("/api/person").when(POST).return(200)
                  ).with(new MockService(8091)
                        .with(new MockEndpoint("/ping").when(GET).return("pong", 200)
                  );
      }
}
```

## JUnit 4

```java
@Rule
public PrivateKayServerRule privateKay = new PrivateKayServerRule("localhost", 9090)
    .with(new HttpProxy(8080, "localhost:8081", "serviceA_to_serviceB"))
    .with(new SocketProxy(8070, "localhost", 8071, "socketA_to_socketB"))
    .with(new MockService(8090)
        .with(new MockEndpoint("/api/version").when(GET).return("<version>2.1.3</version>", 200)
        .with(new MockEndpoint("/api/error").when(ANY).return(500)
        .with(new MockEndpoint("/api/person").when(POST).return(200)
    ).with(new MockService(8091)
        .with(new MockEndpoint("/ping").when(GET).return("pong", 200)
    );
```

Tests can interrogate Private Kay to find out what happened during the execution of the test.

It's possible to test the interactions between services by collecting the interaction by index:

```java
assertThat( privateKay.interactionCount("serviceA_to_serviceB") ).isEqualTo(1);
assertThat( privateKay.interaction("serviceA_to_serviceB", 0).header("Content-Type") ).isEqualTo("application/json");
assertThat( privateKay.interaction("serviceA_to_serviceB", 0).body() ).contains("Jumbalaya");
```

Note that the interaction index is based on when Private Kay saw the interaction.  If there are multiple interactions in an
asychronous system, they may not be in the same order.  In which case you might want to filter first:

```java
Optional<Interaction> interaction = privateKay.interactions().stream()
                                              .findFirst( i -> i.body().contains("Jumbalaya") );
```

TCP socket interactions are stored as an array of bytes.  If you are able to delimit data being sent down the pipe, then you
need to provide a delimiter to Private Kay.  The delimiter returns true when the end of a message has been reached.  Each time
new data is received, it is passed to the delimiter function to check whether the message is complete.  When the message is
considered complete, Private Kay will mark the bytes as a new message.  Messages can then be retrieved by index.
```java
@Test
void messageLengthShouldBe8ForInitialMessage() {
    privateKay.get("socketA_to_socketB").delimiter( bytes -> bytes[bytes.length] == 0 );
    ...
    assertThat( privateKay.interaction("socketA_to_socketB", 0).bytes().length ).isEqualTo(8);
}
```

It's possible to override endpoint for specific tests using the `with` methods:

```java
@Test
void shouldDoSomethingSensibleIfVersionDoesNotRespond() {
    privateKay.with(new MockEndpoint("/api/version")).when(GET).return(503);
    ...
}
```

## Considerations

When using Private Kay as a proxy, bear in mind that running tests in parallel will negate any sensible ability to
work out which connection stream was caused by which test, meaning it basically becomes impossible to make assertions
on the network content.
