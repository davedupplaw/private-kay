package uk.dupplaw.david.privatekay.server

import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.domain.HttpVerb

interface ResponseGenerator<T> {
    fun generateResponse(requestType: HttpVerb, requestPath: String): ResponseEntity<T>
}
