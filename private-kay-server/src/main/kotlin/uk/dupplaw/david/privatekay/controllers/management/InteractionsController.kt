package uk.dupplaw.david.privatekay.controllers.management

import org.springframework.context.annotation.Profile
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.CapturedRequestsAndResponses
import uk.dupplaw.david.privatekay.server.util.extractPathFromPattern
import javax.servlet.http.HttpServletRequest

@RestController
@Profile("management")
internal class InteractionsController(
        private val store: CapturedRequestsAndResponses
) {
    @GetMapping("api/interactions/{type}/**")
    fun interactions(servletPath: HttpServletRequest,
                     @PathVariable("type") type: String): ResponseEntity<*> {
        return ResponseEntity.ok(
                store.getRequestResponses(
                        servletPath.extractPathFromPattern(),
                        HttpVerb.valueOf(type)
                )
        )
    }

    @DeleteMapping("api/interactions")
    fun clearInteractions() = store.clearAll()
}
