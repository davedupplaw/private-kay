package uk.dupplaw.david.privatekay.controllers.management

import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.RequestResponseMappings
import uk.dupplaw.david.privatekay.server.util.extractPathFromPattern
import java.net.MalformedURLException
import java.net.URL
import javax.servlet.http.HttpServletRequest

@RestController
@Profile("management")
internal class ProxyController(
        private val mappings: RequestResponseMappings
) {
    @PostMapping("api/proxy/{method}/**")
    fun proxy(
            servletPath: HttpServletRequest,
            @PathVariable("method") method: String,
            @RequestBody serviceUrl: String
    ): ResponseEntity<*> {
        return try {
            URL(serviceUrl) // check the URL is valid
            mappings.addProxiedResponse(servletPath.extractPathFromPattern(), HttpVerb.valueOf(method), serviceUrl)
            ResponseEntity.ok("OK")
        } catch (mue: MalformedURLException) {
            ResponseEntity("'$serviceUrl' is not a valid URL", HttpStatus.BAD_REQUEST)
        }
    }
}
