package uk.dupplaw.david.privatekay.controllers.management

import org.springframework.context.annotation.Profile
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Profile("management")
internal class VersionController {
    @GetMapping("api/version")
    fun version(): ResponseEntity<String> = ResponseEntity.ok(
            ProxyController::class.java.getResource("/version/version.txt").readText()
    )
}
