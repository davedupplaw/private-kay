package uk.dupplaw.david.privatekay.controllers.management

import mu.KotlinLogging
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.RequestResponseMappings
import uk.dupplaw.david.privatekay.server.util.extractPathFromPattern
import javax.servlet.http.HttpServletRequest

@RestController
@Profile("management")
internal class MockingController(
        private val mappings: RequestResponseMappings
) {
    val logger = KotlinLogging.logger {}

    @PostMapping("api/mock/{type}/{status}/**")
    fun map(servletPath: HttpServletRequest,
            @PathVariable("type") type: String,
            @PathVariable("status") status: Int,
            @RequestBody requestBody: String): ResponseEntity<*> {
        return try {
            val verb = HttpVerb.valueOf(type.toUpperCase())
            mappings.addMockedResponse(servletPath.extractPathFromPattern(), verb, status, requestBody)
            logger.info {"Mapping now $mappings"}
            ResponseEntity.ok("OK")
        } catch (iae: IllegalArgumentException) {
            ResponseEntity("Unknown HTTP Verb $type", HttpStatus.BAD_REQUEST)
        }
    }
}

