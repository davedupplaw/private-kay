package uk.dupplaw.david.privatekay.domain

enum class HttpVerb {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    HEAD,
    OPTIONS,
    TRACE
}
