package uk.dupplaw.david.privatekay

import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import uk.dupplaw.david.privatekay.server.CapturedRequestsAndResponses
import uk.dupplaw.david.privatekay.server.RequestResponseMapper
import uk.dupplaw.david.privatekay.server.RequestResponseMappings


@Configuration
@EnableAutoConfiguration
@ComponentScan("uk.dupplaw.david.privatekay.server")
open class PrivateKayRootConfiguration {
    @Bean
    open fun requestResponseMapper() = RequestResponseMapper(requestResponseMappings())

    @Bean
    open fun requestResponseMappings() = RequestResponseMappings()

    @Bean
    open fun capturedRequestsAndResponses() = CapturedRequestsAndResponses()
}

@Configuration
@EnableAutoConfiguration
@ComponentScan("uk.dupplaw.david.privatekay.controllers.api")
open class PrivateKayApiConfiguration

@Configuration
@EnableAutoConfiguration
@ComponentScan("uk.dupplaw.david.privatekay.controllers.management")
open class PrivateKayManagementConfiguration

@SpringBootApplication
open class PrivateKay {
    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            SpringApplicationBuilder()
                    .parent(PrivateKayRootConfiguration::class.java)
                    .web(WebApplicationType.NONE)
                    .child(PrivateKayApiConfiguration::class.java)
                    .web(WebApplicationType.SERVLET)
                    .profiles("api")
                    .sibling(PrivateKayManagementConfiguration::class.java)
                    .web(WebApplicationType.SERVLET)
                    .profiles("management")
                    .run(*args)
        }
    }
}
