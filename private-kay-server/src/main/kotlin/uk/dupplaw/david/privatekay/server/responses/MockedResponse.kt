package uk.dupplaw.david.privatekay.server.responses

import mu.KotlinLogging
import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.ResponseGenerator

class MockedResponse<T>(
        val mockResponse: T,
        val status: Int = 200
): ResponseGenerator<T> {
    private val logger = KotlinLogging.logger {}

    override fun generateResponse(requestType: HttpVerb, requestPath: String)
            : ResponseEntity<T> {
        logger.info { "Mocking response to $requestPath" }
        return ResponseEntity.status(status).body(mockResponse)
    }
}

