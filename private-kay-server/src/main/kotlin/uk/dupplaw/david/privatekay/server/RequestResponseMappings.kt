package uk.dupplaw.david.privatekay.server

import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.domain.RequestResponseMapping
import uk.dupplaw.david.privatekay.server.responses.MockedResponse
import uk.dupplaw.david.privatekay.server.responses.ProxyResponse

data class RequestResponseMappings(
        val mappings: MutableMap<HttpVerb, MutableMap<String, RequestResponseMapping>> = mutableMapOf()
) {
    fun addMockedResponse(servletPath: String, verb: HttpVerb, status: Int, requestBody: String) {
        mappings.getOrPut(verb, { mutableMapOf() })[servletPath] =
                RequestResponseMapping(verb, servletPath, MockedResponse(requestBody, status))
    }

    fun addProxiedResponse(servletPath: String, verb: HttpVerb, url: String) {
        mappings.getOrPut(verb, { mutableMapOf() })[servletPath] =
                RequestResponseMapping(verb, servletPath, ProxyResponse(url))
    }

    fun get(verb: HttpVerb, path: String) = mappings[verb]?.get(path)
    fun size() = mappings.size
}
