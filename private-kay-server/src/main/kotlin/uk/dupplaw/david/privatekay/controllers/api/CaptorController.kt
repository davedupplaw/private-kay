package uk.dupplaw.david.privatekay.controllers.api

import mu.KotlinLogging
import org.springframework.context.annotation.Profile
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.CapturedRequestsAndResponses
import uk.dupplaw.david.privatekay.server.RequestResponse
import uk.dupplaw.david.privatekay.server.RequestResponseMapper
import javax.servlet.http.HttpServletRequest

@RestController
@Profile("api")
class CaptorController(
        private val mapper: RequestResponseMapper,
        private val store: CapturedRequestsAndResponses
) {
    private val logger = KotlinLogging.logger {}

    @RequestMapping(
            method = [
                RequestMethod.GET, RequestMethod.POST,
                RequestMethod.DELETE, RequestMethod.PUT,
                RequestMethod.PATCH, RequestMethod.HEAD,
                RequestMethod.TRACE, RequestMethod.OPTIONS],
            value = ["/**", "/**/**"]
    )
    fun capture(request: HttpServletRequest,
                requestEntity: RequestEntity<*>): ResponseEntity<*> {
        logger.info { "Capturing request $request" }
        logger.info { "${mapper.requestResponseMappings}" }
        val response = mapper.responseFor(
                HttpVerb.valueOf(request.method.toUpperCase()),
                request.servletPath
        )

        store.addRequestResponse(request.servletPath, RequestResponse(requestEntity, response))

        return response
    }
}
