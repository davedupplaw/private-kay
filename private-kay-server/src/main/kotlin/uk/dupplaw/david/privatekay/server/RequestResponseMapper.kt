package uk.dupplaw.david.privatekay.server

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import uk.dupplaw.david.privatekay.domain.HttpVerb

class RequestResponseMapper(
        val requestResponseMappings: RequestResponseMappings
) {
    fun responseFor(verb: HttpVerb, servletPath: String): ResponseEntity<*> =
            requestResponseMappings.get(verb, servletPath)
                    ?.responseGenerator
                    ?.generateResponse(verb, servletPath)
                    ?: ResponseEntity("No Mapping for $servletPath", HttpStatus.NOT_FOUND)
}
