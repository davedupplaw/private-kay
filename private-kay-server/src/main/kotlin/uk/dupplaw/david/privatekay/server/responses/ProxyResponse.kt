package uk.dupplaw.david.privatekay.server.responses

import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.ResponseGenerator

class ProxyResponse(val url: String) : ResponseGenerator<String> {
    private val logger = KotlinLogging.logger {}

    override fun generateResponse(requestType: HttpVerb, requestPath: String): ResponseEntity<String> {
        logger.info { "Proxying request to url" }
        val response = khttp.request(requestType.toString(), url, mapOf(), mapOf())
        return ResponseEntity(response.text, HttpStatus.valueOf(response.statusCode))
    }
}
