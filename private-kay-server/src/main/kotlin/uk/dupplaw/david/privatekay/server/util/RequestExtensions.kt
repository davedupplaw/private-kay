package uk.dupplaw.david.privatekay.server.util

import org.springframework.util.AntPathMatcher
import org.springframework.web.servlet.HandlerMapping
import javax.servlet.http.HttpServletRequest

fun HttpServletRequest.extractPathFromPattern(): String {
    val path = this.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) as String
    val bestMatchPattern = this.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) as String
    return "/${AntPathMatcher().extractPathWithinPattern(bestMatchPattern, path)}"
}
