package uk.dupplaw.david.privatekay.domain

import uk.dupplaw.david.privatekay.server.ResponseGenerator

data class RequestResponseMapping(
        var requestType: HttpVerb,
        var requestPath: String,
        var responseGenerator: ResponseGenerator<*>
)





