package uk.dupplaw.david.privatekay.server

import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import uk.dupplaw.david.privatekay.domain.HttpVerb

data class RequestResponse(
        val request: RequestEntity<*>,
        val response: ResponseEntity<*>
)

class CapturedRequestsAndResponses(
        val store: MutableMap<HttpVerb, MutableMap<String, MutableList<RequestResponse>>> = mutableMapOf()
) {
    fun addRequestResponse(path: String, requestResponse: RequestResponse) {
        val verb = requestResponse.request.method?.toString()?.toUpperCase() ?: "GET"

        store.getOrPut(HttpVerb.valueOf(verb), { mutableMapOf() })
                .getOrPut(path, { mutableListOf() })
                .add(requestResponse)
    }

    fun getRequests(path: String, verb: HttpVerb) =
            store[verb]?.get(path)?.map { rr -> rr.request } ?: listOf()

    fun getResponses(path: String, verb: HttpVerb) =
            store[verb]?.get(path)?.map { rr -> rr.response } ?: listOf()

    fun getRequestResponses(path: String, verb: HttpVerb) =
            store[verb]?.get(path) ?: listOf()

    val interactionCount: Int
        get() = store.map { (_, map) -> map.map { (_, list) -> list.size }.size }.sum()

    fun clearAll() = store.clear()
}
