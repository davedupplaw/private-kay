package uk.dupplaw.david.privatekay.server

import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.http.HttpStatus
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.responses.ProxyResponse
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ProxyResponseFunctionalTest {
    private val testUtils = TestUtils()
    private val notUsed = ""
    private val contentFolder = File("./src/test/resources/nginx")

    @Rule var nginx: KNginxContainer = KNginxContainer()
            .withCustomContent(contentFolder.toString())
            .waitingFor(HttpWaitStrategy().forPort(80)
                    .forPath("/").forStatusCode(200))

    private val randomUrl = "http://3o4it4joi4g"

    @BeforeAll
    fun `start nginx`() {
        nginx.start()
    }

    @Test
    fun `should store the passed in url`() {
        val unit = ProxyResponse(randomUrl)
        assertThat(unit.url).isSameAs(randomUrl)
    }

    @Test
    fun `should return the text of the given url`() {
        val expectedContent = ProxyResponseFunctionalTest::class
                .java.getResource("/nginx/index.html").readText()
        val url = nginx.getBaseUrl("http", 80).toString()

        val unit = ProxyResponse(url)
        assertThat(unit.url).isEqualTo(url)

        val response = unit.generateResponse(HttpVerb.GET, notUsed)

        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).isEqualTo(expectedContent)
    }

    @Test
    fun `should return the destination status code`() {
        val url = nginx.getBaseUrl("http", 80).toString()

        // nginx returns NOT_FOUND for stuff it doesn't know about
        val unit = ProxyResponse("$url/somethingNotThere.html")

        val response = unit.generateResponse(HttpVerb.GET, notUsed)

        assertThat(response.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }
}
