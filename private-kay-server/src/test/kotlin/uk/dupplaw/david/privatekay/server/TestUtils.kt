package uk.dupplaw.david.privatekay.server

import mu.KotlinLogging
import org.testcontainers.containers.NginxContainer

class KNginxContainer : NginxContainer<KNginxContainer>()

class TestUtils(
    private val apiPort: Int = 8081,
    private val managementPort: Int = 8082
) {
    private val logger = KotlinLogging.logger {}

    init {
        logger.info { "Using test ports: API $apiPort, management: $managementPort" }
    }

    fun getVersion() =
            khttp.get("http://localhost:$managementPort/api/version")

    fun makeProxy(urlToProxyTo: String, path: String, method: String) =
        khttp.post(
                url  = "http://localhost:$managementPort/api/proxy/$method$path",
                data = urlToProxyTo
        )

    fun makeMock(response: String, path: String, statusCode: Int, method: String) =
            khttp.post(
                    url = "http://localhost:$managementPort/api/mock/$method/$statusCode$path",
                    data = response
            )

    fun clearInteractions() =
            khttp.delete("http://localhost:$managementPort/api/interactions")

    fun clearAllMappings() =
        khttp.delete("http://localhost:$managementPort/private-kay/api/mappings")

    fun makeRequest(path: String, method: String) =
        khttp.request(method, "http://localhost:$apiPort$path").text
}
