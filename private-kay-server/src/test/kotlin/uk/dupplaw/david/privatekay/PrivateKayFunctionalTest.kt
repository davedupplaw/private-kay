package uk.dupplaw.david.privatekay

import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.WebApplicationType
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy
import uk.dupplaw.david.privatekay.client.PrivateKayClient
import uk.dupplaw.david.privatekay.server.KNginxContainer
import java.io.File

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [PrivateKay::class]
)
@ContextConfiguration(classes = [
    PrivateKayRootConfiguration::class,
    PrivateKayManagementConfiguration::class,
    PrivateKayApiConfiguration::class
])
@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PrivateKayFunctionalTest {

    private val contentFolder = File("./src/test/resources/nginx")
    @Rule var nginx: KNginxContainer = KNginxContainer()
            .withCustomContent(contentFolder.toString())
            .waitingFor(HttpWaitStrategy()
                    .forPort(80)
                    .forPath("/")
                    .forStatusCode(200)
            )

    private lateinit var privateKayClient: PrivateKayClient

    @BeforeAll
    fun `start nginx`() {
        val apiAppBuilder = SpringApplicationBuilder()
                .parent(PrivateKayRootConfiguration::class.java)
                .web(WebApplicationType.NONE)
                .child(PrivateKayApiConfiguration::class.java)
                .web(WebApplicationType.SERVLET)
                .profiles("api")
        val apiContext = apiAppBuilder.run()
        val managementContext = apiAppBuilder
                .sibling(PrivateKayManagementConfiguration::class.java)
                .web(WebApplicationType.SERVLET)
                .profiles("management")
                .run()

        val apiPort = apiContext.environment.getProperty("local.server.port", Int::class.java)!!
        val managementPort = managementContext.environment.getProperty("local.server.port", Int::class.java)!!

        privateKayClient = PrivateKayClient(apiPort = apiPort, managementPort = managementPort)
        nginx.start()
    }

    @BeforeEach
    fun `reset service state`() {
        privateKayClient.clearInteractions()
        privateKayClient.clearAllMappings()
    }

    @Test
    fun `should have a version endpoint on the management server`() {
        val response = privateKayClient.getVersion()

        assertThat(response).isEqualTo(
                PrivateKayFunctionalTest::class.java
                        .getResource("/version/version.txt").readText()
        )
    }

    @Test
    fun `Can proxy a response to an external service`() {
        val url = nginx.getBaseUrl("http", 80).toString()

        privateKayClient.makeProxy(url, "/my/nginx/proxy/path", "GET")
        val response = privateKayClient.makeRequest("/my/nginx/proxy/path", "GET")

        assertThat(response).contains("It Works, you demi-god.")
    }

    @Test
    fun `Can mock a response to an external service`() {
        privateKayClient.makeMock("My Mocked Response", "/my/nginx/proxy/path", 200, "GET")
        val response = privateKayClient.makeRequest("/my/nginx/proxy/path", "GET")

        assertThat(response).isEqualTo("My Mocked Response")
    }
}
