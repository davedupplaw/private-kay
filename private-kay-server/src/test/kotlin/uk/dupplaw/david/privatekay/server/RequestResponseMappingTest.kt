package uk.dupplaw.david.privatekay.server

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.domain.RequestResponseMapping
import uk.dupplaw.david.privatekay.server.responses.MockedResponse
import uk.dupplaw.david.privatekay.server.responses.ProxyResponse

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class RequestResponseMappingTest {
    @Test
    fun `should set properties from constructor`() {
        val basicResponse = MockedResponse("thing")
        var unit = RequestResponseMapping( HttpVerb.GET, "/path", basicResponse )

        assertThat( unit.responseGenerator ).isSameAs( basicResponse )
        assertThat( unit.requestPath ).isEqualTo( "/path" )
        assertThat( unit.requestType ).isEqualTo( HttpVerb.GET )

        val proxyResponse = ProxyResponse("http://wibble")
        unit = RequestResponseMapping( HttpVerb.DELETE, "/proxy", proxyResponse )

        assertThat( unit.responseGenerator ).isSameAs( proxyResponse )
        assertThat( unit.requestPath ).isEqualTo( "/proxy" )
        assertThat( unit.requestType ).isEqualTo( HttpVerb.DELETE )
    }

}
