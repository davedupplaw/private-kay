package uk.dupplaw.david.privatekay.server

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.domain.HttpVerb
import java.net.URI

internal class CapturedRequestsAndResponsesTest {

    private val req1 = RequestEntity.get(URI.create("http://example.com")).build()
    private val res1 = ResponseEntity.ok("response-1")
    private val req2 = RequestEntity.post(URI.create("http://localhost")).build()
    private val res2 = ResponseEntity.ok("response-2")
    private val req3 = RequestEntity.get(URI.create("http://example.com")).build()
    private val res3 = ResponseEntity.ok("response-1")

    private val requestResponse1GET = RequestResponse(req1, res1)
    private val requestResponse2POST = RequestResponse(req2, res2)
    private val requestResponse3GET = RequestResponse(req3, res3)

    private val unit = CapturedRequestsAndResponses()

    @Test
    fun `should be empty to start with`() {
        assertThat( unit.store ).isNotNull.isEmpty()
        assertThat( unit.interactionCount ).isEqualTo( 0 )
    }

    @Test
    fun `calling getRequests on an empty set should return an empty list`() {
        assertThat( unit.getRequests("/path", HttpVerb.GET) ).isNotNull.isEmpty()
        assertThat( unit.getRequests("/path", HttpVerb.POST) ).isNotNull.isEmpty()
    }

    @Test
    fun `calling getResponses on an empty set should return an empty list`() {
        assertThat( unit.getResponses("/path", HttpVerb.GET) ).isNotNull.isEmpty()
        assertThat( unit.getResponses("/path", HttpVerb.POST) ).isNotNull.isEmpty()
    }

    @Test
    fun `calling getRequestResponses on an empty set should return an empty list`() {
        assertThat( unit.getRequestResponses("/path", HttpVerb.GET) ).isNotNull.isEmpty()
        assertThat( unit.getRequestResponses("/path", HttpVerb.POST) ).isNotNull.isEmpty()
    }

    @Test
    fun `should be able to add a request response`() {
        val requestResponse = RequestResponse(req1, res1)
        unit.addRequestResponse( "/path", requestResponse)

        assertThat(unit.store).hasSize(1)
        assertThat(unit.interactionCount).isEqualTo(1)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]).isNotNull.isNotEmpty.hasSize(1)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]!![0]).isNotNull.isSameAs(requestResponse)

        assertThat(unit.getRequests("/path", HttpVerb.GET))
                .isNotNull.hasSize(1).containsExactly(req1)
        assertThat(unit.getResponses("/path", HttpVerb.GET))
                .isNotNull.hasSize(1).containsExactly(res1)
        assertThat(unit.getRequestResponses("/path", HttpVerb.GET))
                .isNotNull.hasSize(1).containsExactly(requestResponse)
    }

    @Test
    fun `should be able to add to different paths`() {
        val requestResponse = RequestResponse(req1, res1)
        unit.addRequestResponse( "/path", requestResponse)
        unit.addRequestResponse( "/a/different/path", requestResponse)

        assertThat(unit.store).hasSize(1)
        assertThat(unit.interactionCount).isEqualTo(2)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]).isNotNull.isNotEmpty.hasSize(1)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]!![0]).isNotNull.isSameAs(requestResponse)
        assertThat(unit.store[HttpVerb.GET]!!["/a/different/path"]).isNotNull.isNotEmpty.hasSize(1)
        assertThat(unit.store[HttpVerb.GET]!!["/a/different/path"]!![0]).isNotNull.isSameAs(requestResponse)
    }

    @Test
    fun `should be able to add multiple responses to same path`() {
        unit.addRequestResponse( "/path", requestResponse1GET)
        unit.addRequestResponse( "/path", requestResponse3GET)

        assertThat(unit.store).hasSize(1)
        assertThat(unit.interactionCount).isEqualTo(1)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]).isNotNull.isNotEmpty.hasSize(2)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]!![0]).isNotNull.isSameAs(requestResponse1GET)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]!![1]).isNotNull.isSameAs(requestResponse3GET)

        assertThat(unit.getRequests("/path", HttpVerb.GET))
                .isNotNull.hasSize(2).containsExactly(req1, req3)
        assertThat(unit.getResponses("/path", HttpVerb.GET))
                .isNotNull.hasSize(2).containsExactly(res1, res3)
        assertThat(unit.getRequestResponses("/path", HttpVerb.GET))
                .isNotNull.hasSize(2).containsExactly(requestResponse1GET, requestResponse3GET)
    }

    @Test
    fun `different verbs are different paths`() {
        unit.addRequestResponse( "/path", requestResponse1GET)
        unit.addRequestResponse( "/path", requestResponse2POST)

        assertThat(unit.store).hasSize(2)
        assertThat(unit.interactionCount).isEqualTo(2)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]).isNotNull.isNotEmpty.hasSize(1)
        assertThat(unit.store[HttpVerb.POST]!!["/path"]).isNotNull.isNotEmpty.hasSize(1)
        assertThat(unit.store[HttpVerb.GET]!!["/path"]!![0]).isNotNull.isSameAs(requestResponse1GET)
        assertThat(unit.store[HttpVerb.POST]!!["/path"]!![0]).isNotNull.isSameAs(requestResponse2POST)

        assertThat(unit.getRequests("/path", HttpVerb.GET))
                .isNotNull.hasSize(1).containsExactly(req1)
        assertThat(unit.getResponses("/path", HttpVerb.POST))
                .isNotNull.hasSize(1).containsExactly(res2)
        assertThat(unit.getRequestResponses("/path", HttpVerb.GET))
                .isNotNull.hasSize(1).containsExactly(requestResponse1GET)
        assertThat(unit.getRequestResponses("/path", HttpVerb.POST))
                .isNotNull.hasSize(1).containsExactly(requestResponse2POST)
    }
}
