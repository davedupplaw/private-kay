//package uk.dupplaw.david.privatekay.controllers
//
//import com.nhaarman.mockito_kotlin.doReturn
//import com.nhaarman.mockito_kotlin.mock
//import org.assertj.core.api.Assertions.assertThat
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.TestInstance
//import org.springframework.http.HttpStatus
//import org.springframework.http.RequestEntity
//import org.springframework.http.ResponseEntity
//import org.springframework.web.servlet.HandlerMapping
//import uk.dupplaw.david.privatekay.controllers.api.ProxyController
//import uk.dupplaw.david.privatekay.domain.HttpVerb
//import uk.dupplaw.david.privatekay.server.*
//import uk.dupplaw.david.privatekay.server.responses.MockedResponse
//import uk.dupplaw.david.privatekay.server.responses.ProxyResponse
//import java.net.URI
//import javax.servlet.http.HttpServletRequest
//
//@TestInstance(TestInstance.Lifecycle.PER_METHOD)
//internal class ProxyControllerTest {
//    private val req1 = RequestEntity.get(URI.create("http://example.com")).build()
//    private val res1 = ResponseEntity.ok("response-1")
//    private val req2 = RequestEntity.post(URI.create("http://localhost")).build()
//    private val res2 = ResponseEntity.ok("response-2")
//    private val req3 = RequestEntity.get(URI.create("http://example.com")).build()
//    private val res3 = ResponseEntity.ok("response-1")
//
//    private val requestResponse1GET = RequestResponse(req1, res1)
//    private val requestResponse2POST = RequestResponse(req2, res2)
//    private val requestResponse3GET = RequestResponse(req3, res3)
//
//    private val requestMappings = RequestResponseMappings()
//    private val requestResponseStore = CapturedRequestsAndResponses()
//    private val unit = ProxyController(requestMappings, requestResponseStore)
//
//    @Test
//    fun `should add the requested mapping as a basic mapping`() {
//        val addMapRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/map/GET/1234/12345"
//            on { method } doReturn "POST"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/map/GET/1234/12345"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/map/{type}/**"
//        }
//
//        val response = unit.map(addMapRequest, "GET", "some response")
//
//        assertThat( requestMappings.size() ).isEqualTo( 1 )
//        assertThat( requestMappings.hasMap("/1234/12345:GET") ).isTrue()
//        assertThat( requestMappings.mappings["/1234/12345:GET"]!!.responseGenerator ).isInstanceOf( MockedResponse::class.java )
//        assertThat( requestMappings.responseFor(HttpVerb.GET, "/1234/12345" ).body ).isEqualTo( "some response" )
//        assertThat( requestMappings.responseFor(HttpVerb.GET, "/1234/12345" ).statusCode ).isEqualTo( HttpStatus.OK )
//        assertThat( response.body ).isEqualTo( "OK" )
//        assertThat( response.statusCode ).isEqualTo( HttpStatus.OK )
//    }
//
//    @Test
//    fun `should be able to add the root to the mappings`() {
//        val addMapRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/map/GET/"
//            on { method } doReturn "POST"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/map/GET/"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/map/{type}/**"
//        }
//
//        val response = unit.map(addMapRequest, "GET", "some response")
//
//        assertThat( requestMappings.size() ).isEqualTo( 1 )
//        assertThat( requestMappings.hasMap("/:GET") ).isTrue()
//        assertThat( requestMappings.responseFor(HttpVerb.GET, "/" ).body ).isEqualTo( "some response" )
//        assertThat( requestMappings.responseFor(HttpVerb.GET, "/" ).statusCode ).isEqualTo( HttpStatus.OK )
//        assertThat( response.body ).isEqualTo( "OK" )
//        assertThat( response.statusCode ).isEqualTo( HttpStatus.OK )
//    }
//
//    @Test
//    fun `should return a 400 Bad Request for an unknown HTTP verb`() {
//        val addMapRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/map/FLUBBER/1234/12345"
//            on { method } doReturn "FLUBBER"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/map/FLUBBER/1234/12345"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/map/{type}/**"
//        }
//
//        val response = unit.map(addMapRequest, "FLUBBER", "some response")
//
//        assertThat(response.body).isEqualTo("Unknown HTTP Verb FLUBBER")
//        assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
//    }
//
//    @Test
//    fun `should add proxy mapping into mappings`() {
//        val addProxyRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/proxy/GET/1234/6533"
//            on { method } doReturn "POST"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/proxy/GET/1234/6533"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/proxy/{type}/**"
//        }
//
//        val response = unit.proxy(addProxyRequest, "GET", "http://localhost:2018/my/service/rocks")
//
//        assertThat( requestMappings.size() ).isEqualTo( 1 )
//        assertThat( requestMappings.hasMap("/1234/6533:GET") ).isTrue()
//        assertThat( requestMappings.mappings["/1234/6533:GET"]!!.responseGenerator ).isInstanceOf( ProxyResponse::class.java )
//        assertThat( (requestMappings.mappings["/1234/6533:GET"]!!.responseGenerator as ProxyResponse).url )
//                .isEqualTo( "http://localhost:2018/my/service/rocks" )
//        assertThat( response.body ).isEqualTo( "OK" )
//        assertThat( response.statusCode ).isEqualTo( HttpStatus.OK )
//    }
//
//    @Test
//    fun `should return bad request if given request body is not a url when adding a proxy`() {
//        val addProxyRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/proxy/GET/1234/6533"
//            on { method } doReturn "POST"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/proxy/GET/1234/6533"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/proxy/{type}/**"
//        }
//
//        val response = unit.proxy(addProxyRequest, "GET", "THIS is !!!&& Not a @@ Valid -- URL {}{[]")
//
//        assertThat( response.body ).isEqualTo( "'THIS is !!!&& Not a @@ Valid -- URL {}{[]' is not a valid URL" )
//        assertThat( response.statusCode ).isEqualTo( HttpStatus.BAD_REQUEST )
//    }
//
//    @Test
//    fun `should be able to retrieve interactions for a path`() {
//        requestResponseStore.addRequestResponse("/1234/6533", requestResponse1GET)
//        requestResponseStore.addRequestResponse("/1234/6533", requestResponse2POST)
//        requestResponseStore.addRequestResponse("/1234/6533", requestResponse3GET)
//
//        val interactionsRequest = mock<HttpServletRequest> {
//            on { servletPath } doReturn "/api/interactions/POST/1234/6533"
//            on { method } doReturn "GET"
//            on { getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) } doReturn "/api/interactions/POST/1234/6533"
//            on { getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE) } doReturn "/api/interactions/{type}/**"
//        }
//
//        var response = unit.interactions(interactionsRequest, "GET")
//
//        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(response.body).isInstanceOf(List::class.java)
//        assertThat(response.body as List<*>).containsExactly(requestResponse1GET, requestResponse3GET)
//
//        response = unit.interactions(interactionsRequest, "POST")
//
//        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(response.body).isInstanceOf(List::class.java)
//        assertThat(response.body as List<*>).containsExactly(requestResponse2POST)
//    }
//
//    @Test
//    fun `should be able to get the contents of the version file`() {
//        val response = unit.version()
//
//        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(response.body).isEqualTo("This is the version file")
//    }
//}
