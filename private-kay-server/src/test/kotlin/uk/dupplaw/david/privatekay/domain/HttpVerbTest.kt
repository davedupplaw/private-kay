package uk.dupplaw.david.privatekay.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

import uk.dupplaw.david.privatekay.domain.HttpVerb.*

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class HttpVerbTest {
    @Test
    fun `should have the expected elements`() {
        assertThat(values())
                .hasSize(8)
                .containsExactly(GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS, TRACE)
    }
}
