package uk.dupplaw.david.privatekay.server

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.responses.MockedResponse

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class MockedResponseTest {
    private val unit = MockedResponse( "my response" )

    @Test
    fun `should set the response from the constructor`() {
        assertThat( unit.mockResponse ).isEqualTo( "my response" )
    }

    @Test
    fun `should return a 200 response entity with the response as a body`() {
        val generatedResponse = unit.generateResponse(HttpVerb.DELETE, "/some/path")

        assertThat(generatedResponse).isNotNull.isInstanceOf( ResponseEntity::class.java )
        assertThat(generatedResponse.statusCode).isEqualTo( HttpStatus.OK )
        assertThat(generatedResponse.body).isEqualTo( "my response" )
    }
}
