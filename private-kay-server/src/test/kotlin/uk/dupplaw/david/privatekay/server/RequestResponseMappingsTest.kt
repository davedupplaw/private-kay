//package uk.dupplaw.david.privatekay.server
//
//import org.assertj.core.api.Assertions.assertThat
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.TestInstance
//import uk.dupplaw.david.privatekay.domain.HttpVerb
//import uk.dupplaw.david.privatekay.domain.RequestResponseMapping
//import uk.dupplaw.david.privatekay.server.responses.MockedResponse
//import uk.dupplaw.david.privatekay.server.responses.ProxyResponse
//
//@TestInstance(TestInstance.Lifecycle.PER_METHOD)
//internal class RequestResponseMappingsTest {
//    private val unit = RequestResponseMappings()
//
//    @Test
//    fun `should have an empty list on start up`() {
//        assertThat( unit.mappings ).isNotNull.hasSize( 0 )
//    }
//
//    @Test
//    fun `should be able to add a mapping between a verb-path combi and a response`() {
//        unit.addMap( "/my/path", HttpVerb.GET, "my response" )
//
//        assertThat( unit.mappings ).isNotNull.hasSize(1)
//        assertThat( unit.mappings["/my/path:GET"] ).isNotNull.isInstanceOf( RequestResponseMapping::class.java )
//        assertThat( unit.mappings["/my/path:GET"]!!.responseGenerator ).isNotNull.isInstanceOf( MockedResponse::class.java )
//        assertThat( (unit.mappings["/my/path:GET"]!!.responseGenerator as MockedResponse).mockResponse ).isNotNull.isEqualTo( "my response" )
//
//        unit.addMap( "/another/path", HttpVerb.POST, "a different response" )
//
//        assertThat( unit.mappings ).isNotNull.hasSize(2)
//        assertThat( unit.mappings["/another/path:POST"] ).isNotNull.isInstanceOf( RequestResponseMapping::class.java )
//        assertThat( unit.mappings["/another/path:POST"]!!.responseGenerator ).isNotNull.isInstanceOf( MockedResponse::class.java )
//        assertThat( (unit.mappings["/another/path:POST"]!!.responseGenerator as MockedResponse).mockResponse ).isNotNull.isEqualTo( "a different response" )
//    }
//
//    @Test
//    fun `should be able to add a proxy mapping between a verb-path comi and a url`() {
//        unit.addProxy( "/my/proxy/path", HttpVerb.GET, "http://localhost:1234/some/api" )
//
//        assertThat( unit.mappings ).isNotNull.hasSize(1)
//        assertThat( unit.mappings["/my/proxy/path:GET"] ).isNotNull.isInstanceOf( RequestResponseMapping::class.java )
//        assertThat( unit.mappings["/my/proxy/path:GET"]!!.responseGenerator ).isNotNull.isInstanceOf( ProxyResponse::class.java )
//        assertThat( (unit.mappings["/my/proxy/path:GET"]!!.responseGenerator as ProxyResponse).url ).isEqualTo( "http://localhost:1234/some/api" )
//    }
//}
