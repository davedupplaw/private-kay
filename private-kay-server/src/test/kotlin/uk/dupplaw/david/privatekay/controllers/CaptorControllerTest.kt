package uk.dupplaw.david.privatekay.controllers

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.MockitoAnnotations.initMocks
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import uk.dupplaw.david.privatekay.controllers.api.CaptorController
import uk.dupplaw.david.privatekay.domain.HttpVerb
import uk.dupplaw.david.privatekay.server.CapturedRequestsAndResponses
import uk.dupplaw.david.privatekay.server.RequestResponse
import uk.dupplaw.david.privatekay.server.RequestResponseMapper
import javax.servlet.http.HttpServletRequest

@ExtendWith(MockKExtension::class)
class CaptorControllerTest {
    @MockK private lateinit var mockMapper: RequestResponseMapper
    @MockK private lateinit var mockStore: CapturedRequestsAndResponses

    @MockK private lateinit var mockHttpGETRequest: HttpServletRequest
    @MockK private lateinit var mockHttpGETRequestEntity: RequestEntity<String>
    @MockK private lateinit var mockResponse: ResponseEntity<*>

    private val servletPath = "/some/path"

    private lateinit var unit: CaptorController

    @BeforeEach
    fun setUp() {
        initMocks(this)

        every { mockHttpGETRequest.servletPath } returns servletPath
        every { mockHttpGETRequest.method } returns "GET"
        every { mockMapper.responseFor(HttpVerb.GET, servletPath) } returns mockResponse
        every { mockStore.addRequestResponse(servletPath, any()) } returns Unit

        unit = CaptorController(mockMapper, mockStore)
    }

    @Test
    fun `Delegates to the mapper to get the response for the request`() {
        unit.capture(mockHttpGETRequest, mockHttpGETRequestEntity)

        verify { mockMapper.responseFor(HttpVerb.GET, servletPath) }
    }

    @Test
    fun `Delegates to the store to store the request response pair`() {
        unit.capture(mockHttpGETRequest, mockHttpGETRequestEntity)

        verify { mockStore.addRequestResponse(servletPath, RequestResponse(mockHttpGETRequestEntity, mockResponse)) }
    }

    @Test
    internal fun `Returns the response`() {
        assertThat(unit.capture(mockHttpGETRequest, mockHttpGETRequestEntity)).isEqualTo(mockResponse)
    }
}
